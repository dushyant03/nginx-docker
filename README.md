#  Docker image vulnerability scan project

# Description

 - This is pipeline created on gitlab using the gitlab-runner service, the basic principle of this pipeline is to build a image and push it to a   staging repo and then scan the image using the anchore service for vulnerabilities,then push the images that pass the scan to a prod repo.

# Components

 - nginx-docker Repo

   This repo consits of the Dokerfile to build the nginx image from source on a Alpine base image (The actual assignment)

 - nginx-vuln Repo

   This repo consists of a custom image, in order to showcase the failure of the scan in case of the scan not passing.

 - staging-images Repo

   This repo is where the initial image is pushed to brfore the scan

 - prod-images Repo

   This repo is where the final image is pushed post passing the scan


# Running of the Build

  Any change made on the either of the nginx repos triggeres the pipeline via the gitlab-runner installed locally.

  - The image is built using the Dockerfile and pushed to the staging-images repo
  - The image is scanned by the anchore application which is running locally.
  - If the image passes the scan, it is pushed to the prod-images repo else the build terminates there.
  - The build takes about 12 minutes to complete


# Verifying the image.

  - docker pull registry.gitlab.com/dushyant03/prod-images/nginx-docker
  - docker run -d -p 80:80 registry.gitlab.com/dushyant03/prod-images/nginx-docker
  




